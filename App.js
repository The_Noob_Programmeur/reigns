import React from 'react';
import { View, StyleSheet, StatusBar, Text } from 'react-native';
import Swip from './Components/Swiper.js';
import GaugeBar from './Components/GaugeBar';
import RestartView from './Components/RestartView';
import StatsView from './Components/StatsView';
import FadeInView from './Components/FadeInView';
import Mock from './mock.json';
import * as Font from 'expo-font';
import { getCards } from './BaseAPI/CardAPI';

export default class App extends React.Component {
  state = {
    listCard: Mock,
    playerStats: {
      argent: 0,
      religion: 0,
      armee: 0,
      population: 0
    },
    yearsInPower: 0,
    statsHasExceed: false,
    cardActive: {},
    fontLoaded: false,
    indicators: {
      religion: false,
      argent: false,
      population: false,
      armee: false
    },
    restartViewIsVisible: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      ForcedSquare: require('./assets/fonts/ForcedSquare.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  componentWillMount() {
    // // Uncomment when API + Data card are ready
    // getCards().then(response => {
    //   this.setState({ listCard: response });
    // });
    const { listCard } = this.state;
    this.setState({ cardActive: listCard[0] });
  }

  setStatsAccepted = cardIndex => {
    const { listCard } = this.state;
    const { playerStats } = { ...this.state };
    const card = listCard[cardIndex];
    this.setState({ cardActive: card });

    const currentState = playerStats;

    for (let [key, value] of Object.entries(card.acceptEffect)) {
      if (currentState[key] + value <= 0) {
        currentState[key] = 0;
      } else if (currentState[key] + value >= 100) {
        currentState[key] = 100;
        this.setState({ statsHasExceed: true });
      } else {
        currentState[key] += value;
      }

      this.setState({ playerStats: currentState });
    }
  };

  setStatsRefused = cardIndex => {
    const { listCard } = this.state;
    const { playerStats } = { ...this.state };
    const card = listCard[cardIndex];
    this.setState({ cardActive: card });

    const currentState = playerStats;

    for (let [key, value] of Object.entries(card.acceptEffect)) {
      if (currentState[key] + value <= 0) {
        currentState[key] = 0;
      } else if (currentState[key] + value >= 100) {
        currentState[key] = 100;
        this.setState({ statsHasExceed: true });
      } else {
        currentState[key] += value;
      }

      this.setState({ playerStats: currentState });
    }
  };

  addYearsInPower = () => {
    const { yearsInPower } = this.state;
    this.setState({ yearsInPower: yearsInPower + 1 });
  };

  resetIndicator = () => {
    const { indicators } = this.state;
    const newIndicators = indicators;
    for (let [key, value] of Object.entries(indicators)) {
      newIndicators[key] = false;
      this.setState({ indicators: newIndicators });
    }
  };

  showIndicator = x => {
    const { cardActive, indicators } = this.state;
    const newIndicators = indicators;

    if (x < 0) {
      this.resetIndicator();
      for (let [key, value] of Object.entries(cardActive.refuseEffect)) {
        if (value > 0) {
          newIndicators[key] = true;
        }
        this.setState({ indicators: newIndicators });
      }
    } else if (x > 0) {
      this.resetIndicator();
      for (let [key, value] of Object.entries(cardActive.acceptEffect)) {
        if (value > 0) {
          newIndicators[key] = true;
        }
        this.setState({ indicators: newIndicators });
      }
    }
  };

  restartGame = () => {
    this.setState({ state: this.state });
  };

  render() {
    const {
      playerStats,
      listCard,
      statsHasExceed,
      yearsInPower,
      cardActive,
      fontLoaded,
      indicators
    } = this.state;
    return (
      <FadeInView style={styles.container}>
        <StatusBar hidden />

        <GaugeBar playerStats={playerStats} indicators={indicators}></GaugeBar>
        {fontLoaded ? (
          <Text style={styles.textDescription}>{cardActive.description}</Text>
        ) : null}
        <Swip
          onAccept={this.setStatsAccepted}
          onRefuse={this.setStatsRefused}
          addYearsInPower={this.addYearsInPower}
          resetIndicator={this.resetIndicator}
          showIndicator={this.showIndicator}
          listCard={listCard}
          cardActive={cardActive}
          yearsInPower={yearsInPower}
          indicators={indicators}
        />
        {fontLoaded ? (
          <Text style={styles.textCharacter}>{cardActive.characterName}</Text>
        ) : null}
        <StatsView yearsInPower={yearsInPower}></StatsView>

        <RestartView
          statsHasExceed={statsHasExceed}
          restartGame={this.restartGame}
        ></RestartView>
      </FadeInView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#353535',
    height: '100%',
    display: 'flex'
  },
  textDescription: {
    marginTop: 30,
    margin: 10,
    fontSize: 35,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: 'ForcedSquare'
  },
  textCharacter: {
    fontSize: 20,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: 355,
    fontFamily: 'ForcedSquare'
  }
});
