const axios = require('axios');
import { API_URL, API_SECRET } from 'react-native-dotenv';

export function getCards() {
  axios({
    method: 'get',
    url: API_URL,
    headers: {
      'x-api-key': API_SECRET,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
    .then(response => {
      return response;
    })
    .catch(err => {
      console.log(err);
    });
}
