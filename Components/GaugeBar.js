import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import ArmyIcon from '../assets/icons/army.png';
import PeopleIcon from '../assets/icons/people.png';
import DollarIcon from '../assets/icons/dollar.png';
import PrayIcon from '../assets/icons/pray.png';

export default class Swip extends React.Component {
  render() {
    const { playerStats, indicators } = this.props;

    return (
      <View style={styles.container}>
        {indicators.argent ? <View style={styles.circleArgent} /> : null}
        {indicators.population ? (
          <View style={styles.circlePopulation} />
        ) : null}
        {indicators.armee ? <View style={styles.circleArmee} /> : null}
        {indicators.religion ? <View style={styles.circleReligion} /> : null}

        <AnimatedCircularProgress
          size={70}
          width={5}
          fill={playerStats.argent}
          backgroundWidth={8}
          tintColor="#F49D37"
          tintColorSecondary="red"
          onAnimationComplete={() => {}}
          backgroundColor="#EAD7D1"
          style={styles.gauge}
          arcSweepAngle={280}
          rotation={40}
          lineCap="round"
        >
          {fill => <Image style={styles.img} source={PeopleIcon}></Image>}
        </AnimatedCircularProgress>
        <AnimatedCircularProgress
          size={70}
          width={5}
          fill={playerStats.population}
          backgroundWidth={8}
          tintColor="#F49D37"
          tintColorSecondary="red"
          onAnimationComplete={() => {}}
          backgroundColor="#EAD7D1"
          style={styles.gauge}
          arcSweepAngle={280}
          rotation={40}
          lineCap="round"
        >
          {fill => <Image style={styles.img} source={DollarIcon}></Image>}
        </AnimatedCircularProgress>
        <AnimatedCircularProgress
          size={70}
          width={5}
          fill={playerStats.religion}
          backgroundWidth={8}
          tintColor="#F49D37"
          tintColorSecondary="red"
          onAnimationComplete={() => {}}
          backgroundColor="#EAD7D1"
          style={styles.gauge}
          arcSweepAngle={280}
          rotation={40}
          lineCap="round"
        >
          {fill => <Image style={styles.img} source={PrayIcon}></Image>}
        </AnimatedCircularProgress>
        <AnimatedCircularProgress
          size={70}
          width={5}
          fill={playerStats.armee}
          backgroundWidth={8}
          tintColor="#F49D37"
          tintColorSecondary="red"
          onAnimationComplete={() => {}}
          backgroundColor="#EAD7D1"
          style={styles.gauge}
          arcSweepAngle={280}
          rotation={40}
          lineCap="round"
        >
          {fill => <Image style={styles.img} source={ArmyIcon}></Image>}
        </AnimatedCircularProgress>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '15%',
    backgroundColor: '#221E22',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowOffset: { width: 2, height: 1 },
    shadowColor: 'black',
    shadowOpacity: 1.0
  },
  text: {
    fontSize: 30,
    color: 'white',
    zIndex: 5,
    marginRight: 20
  },
  gauge: {
    marginRight: 10,
    marginLeft: 10
  },
  img: {
    width: 30,
    height: 30
  },
  circleArgent: {
    width: 10,
    height: 10,
    borderRadius: 100 / 2,
    backgroundColor: '#EAD7D1',
    position: 'absolute',
    top: 18,
    left: 138
  },
  circlePopulation: {
    width: 10,
    height: 10,
    borderRadius: 100 / 2,
    backgroundColor: '#EAD7D1',
    marginLeft: 30,
    position: 'absolute',
    top: 18,
    left: 17
  },
  circleArmee: {
    width: 10,
    height: 10,
    borderRadius: 100 / 2,
    backgroundColor: '#EAD7D1',
    marginLeft: 60,
    position: 'absolute',
    top: 18,
    left: 320
  },
  circleReligion: {
    width: 10,
    height: 10,
    borderRadius: 100 / 2,
    backgroundColor: '#EAD7D1',
    marginLeft: 90,
    position: 'absolute',
    left: 138,
    top: 18
  }
});
