import React from 'react';
import { StyleSheet } from 'react-native';

import FadeInView from './Components/FadeInView';

export default class App extends React.Component {
  componentDidMount() {}

  render() {
    const {} = this.state;
    return (
      <FadeInView style={styles.container}>
        <Text>Team 2 present..</Text>
      </FadeInView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#353535',
    height: '100%',
    display: 'flex'
  },
  textDescription: {
    marginTop: 30,
    margin: 10,
    fontSize: 35,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: 'ForcedSquare'
  },
  textCharacter: {
    fontSize: 20,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: 355,
    fontFamily: 'ForcedSquare'
  }
});
