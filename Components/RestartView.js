import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Button, Dialog } from 'react-native-paper';
import * as Font from 'expo-font';

export default class RestartView extends React.Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      ForcedSquare: require('../assets/fonts/ForcedSquare.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const { statsHasExceed, restartGame } = this.props;
    const { fontLoaded } = this.state;
    return (
      <Dialog visible={statsHasExceed} style={styles.container}>
        {fontLoaded ? (
          <>
            <Text style={styles.text}>You failed as a King..</Text>
            <Button
              mode="contained"
              onPress={() => restartGame()}
              style={styles.buttonContainer}
              contentStyle={styles.button}
              labelStyle={styles.label}
              color={'white'}
            >
              <Text>Try again !</Text>
            </Button>
          </>
        ) : null}
      </Dialog>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 300,
    borderRadius: 20,
    backgroundColor: '#353535'
  },
  text: {
    textAlign: 'center',
    fontSize: 30,
    backgroundColor: 'transparent',
    color: 'darkgrey',
    marginVertical: 20,
    fontFamily: 'ForcedSquare'
  },
  button: {
    width: 200,
    height: 50,
    borderRadius: 20,
    fontFamily: 'ForcedSquare'
  },
  buttonContainer: {
    width: 200,
    height: 50,
    marginBottom: 10,
    marginTop: 30,
    borderRadius: 20,
    backgroundColor: '#F49D37',
    shadowOffset: { width: 2, height: 1 },
    shadowColor: 'black',
    shadowOpacity: 1.0
  },
  label: {
    fontSize: 15,
    fontFamily: 'ForcedSquare'
  }
});
