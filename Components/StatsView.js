import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import ReignIcon from '../assets/icons/reign.png';
import * as Font from 'expo-font';

export default class StatsView extends React.Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      ForcedSquare: require('../assets/fonts/ForcedSquare.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const { yearsInPower } = this.props;
    const { fontLoaded } = this.state;
    return (
      <View style={styles.container}>
        {fontLoaded ? (
          <>
            <Image source={ReignIcon} style={styles.img}></Image>
            <Text style={styles.textYear}>{`${yearsInPower} `}</Text>
            <Text style={styles.text}>years in power</Text>
          </>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '15%',
    backgroundColor: '#221E22',
    flexDirection: 'row',
    position: 'absolute',
    textAlign: 'center',
    justifyContent: 'center',
    flex: 1,
    marginTop: 570
  },
  img: {
    height: 60,
    width: 60,
    margin: 20
  },
  text: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'ForcedSquare',
    marginVertical: 40
  },
  textYear: {
    fontSize: 40,
    color: 'white',
    fontFamily: 'ForcedSquare',
    marginVertical: 35
  }
});
