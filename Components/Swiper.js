import React from 'react';
import { StyleSheet, Text, View, Platform, Image } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import * as Font from 'expo-font';

export default class Swip extends React.Component {
  state = {
    labelAccept: this.props.cardActive.response.accept.label,
    labelRefuse: this.props.cardActive.response.refuse.label,
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      ForcedSquare: require('../assets/fonts/ForcedSquare.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  setCardActiveLabel = () => {
    const { cardActive } = this.props;
    this.setState({ labelRefuse: cardActive.response.refuse.label });
    this.setState({ labelAccept: cardActive.response.accept.label });
  };

  render() {
    const {
      onAccept,
      onRefuse,
      listCard,
      addYearsInPower,
      resetIndicator,
      showIndicator
    } = this.props;
    const { labelAccept, labelRefuse, fontLoaded } = this.state;

    return (
      <View style={styles.container}>
        {fontLoaded ? (
          <Swiper
            cards={listCard}
            useViewOverflow={Platform.OS === 'ios'}
            renderCard={card => {
              return (
                <View>
                  <Image
                    style={styles.img}
                    source={{ uri: `data:image/png;base64, ${card.img}` }}
                  />
                </View>
              );
            }}
            onSwipedLeft={cardIndex => {
              onRefuse(cardIndex + 1);
              addYearsInPower();
            }}
            onSwipedRight={cardIndex => {
              onAccept(cardIndex + 1);
              addYearsInPower();
            }}
            onSwiped={cardIndex => {
              this.setCardActiveLabel(cardIndex);
              resetIndicator();
            }}
            onSwiping={x => {
              showIndicator(x);
            }}
            onSwipedAborted={() => {
              resetIndicator();
            }}
            overlayLabels={{
              left: {
                element: <Text style={styles.textResponse}>{labelRefuse}</Text>,
                title: 'NOPE',
                style: {
                  label: {
                    backgroundColor: 'black',
                    borderColor: 'black',
                    color: 'white',
                    borderWidth: 1,
                    fontSize: 50,
                    fontWeight: 'bold'
                  },
                  wrapper: {
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }
                }
              },
              right: {
                element: <Text style={styles.textResponse}>{labelAccept}</Text>,
                title: 'LIKE',
                style: {
                  label: {
                    backgroundColor: 'black',
                    borderColor: 'black',
                    color: 'white',
                    borderWidth: 1,
                    fontSize: 50,
                    fontWeight: 'bold'
                  },
                  wrapper: {
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }
                }
              }
            }}
            backgroundColor={'#4FD0E9'}
            verticalSwipe={false}
            overlayLabelStyle={styles.overlayLabel}
            overlayLabelWrapperStyle={styles.overlayLabelWrapper}
            containerStyle={styles.container}
            cardStyle={styles.card}
            animateOverlayLabelsOpacity
            animateCardOpacity
            stackSize={3}
            stackSeparation={15}
            cardHorizontalMargin={50}
            // inputRotationRange={[-20 / 2, 0, 20 / 2]}
            // inputCardOpacityRangeX={[-100 / 2, -100 / 3, 0, 100 / 3, 100 / 2]}
            inputOverlayLabelsOpacityRangeX={[-50 / 3, -5 / 4, 0, 5 / 4, 5 / 3]}
            inputRotationRange={[-300 / 2, 0, 300 / 2]}
            outputRotationRange={['-30deg', '0deg', '30deg']}
            overlayOpacityHorizontalThreshold={20 / 4}
            horizontalThreshold={500 / 4}
            verticalThreshold={1 / 5}
          ></Swiper>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  card: {
    flex: 1,
    backgroundColor: '#EAD7D1',
    height: 270,
    width: 270,
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 3,
    marginTop: -20
  },
  img: {
    width: 51,
    height: 51,
    resizeMode: 'contain'
  },
  overlayLabel: {
    fontSize: 50,
    fontWeight: 'bold'
  },
  overlayLabelWrapper: {
    backgroundColor: 'darkgrey',
    height: 60,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  textResponse: {
    fontFamily: 'ForcedSquare',
    fontSize: 30
  }
});
