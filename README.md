# Reigns

## Equipe : Groupe 1

*  Antoine Schulz
*  Sébastien Guidez
*  Nicolas Lepinette
*  Romain Marino
*  Antoine Berthier

## Presentation du projet 
Dans le cadre du cours "Ac'Lab" Nous avons recu un projet a réaliser en groupe avec une methode Agile.

Nous devions proposer une interface mobile du jeu [Reign](https://fr.wikipedia.org/wiki/Reigns) , c’est à dire un simulateur de royaume où le joueur doit répondre à une succession de choix binaires (oui/non). Ces choix auront pour conséquences de faire évoluer 4 jauges (l’armée/la population/la trésorerie/la religion) positivement ou négativement. Le joueur perd la partie si une de ces jauges est au minimum ou au maximum.

## Mise en place du projet 

Pour pouvoir travailler en group sur le projet nous avons mis en place des outils avec des methodologies :

*  [GitLab](https://gitlab.com/The_Noob_Programmeur/reigns) : Depot de versioning pour le traville en colaboration. Nous avons mis palce une methodologie git flot.
```
master                             Rendu Sprint 
---------------------------------------------
         \____________________________/_____
          Dev  \________________/
                 feature/sujet

```
*   [Trello](https://trello.com/invite/b/qM7L3MSY/ebf2924cb11b10aa464ac01445ecfa6b/reigns) : Trello est un outil de gestion de projet qui permet de representer les taches a faire durant le sprint.



## Info technique :

Install :
npm install -g react-native-cli

Execute :
react-native link react-native-svg

react-native link react-native-vector-icons

## Objectif

Le but du projet est de proposer une interface mobile du jeu Reign, c’est à dire un simulateur de royaume où le joueur doit répondre à une succession de choix binaires (oui/non). Ces choix auront pour conséquences de faire évoluer 4 jauges (l’armée/la population/la trésorerie/la religion) positivement ou négativement. Le joueur perd la partie si une de ces jauges est au minimum ou au maximum.
 Déroulement du projet

Dans le cadre de la gestion de projet, nous avons utilisé plusieurs logiciels. Tout d’abord pour stocker nos fichiers sources et nous permettre de travailler sur le projet à plusieurs en même temps, nous avons utilisé Gitlab. Il nous suffit de créer une nouvelle branche afin de ne pas corrompre le travail déjà réalisé et ainsi de garder une source commune pour  tous.
Ayant choisis la méthode de travail agile pour mener à bien ce projet, nous avons utilisé Trello afin de déterminer les tâches à faire et à se les partager. Ainsi, nous avons découpé toutes les tâches en sprints et les membres du groupe se les attribuaient lors des séances de cours communes. 
Nous communiquions et faisions nos comptes-rendus d’évolution du projet lors des séances de cours communes ou via un groupe créé sur Discord.

Le jeu sera développé avec la solution Expo de React, nous permettant d’avoir un client compatible Android et Apple. Le code sera en JavaScript avec le Framework  React-natif. 

## API

L’idée étant de mettre en forme ce qu’a produit au préalable le groupe 3. C’est à dire d’afficher les choix, de permettre la validation du choix (Oui/Non), de faire évoluer les jauges en conséquence et enfin de déclarer la défaite du joueur.
Le groupe 3 a mis en place d’une base de données et des cartes mettant en scène les choix. L’API a une url globale et 4 arguments à cette URL possibles, les cartes, les personnages, les objets et les fins du jeu.
Nous avons eu accès à cette API vers la fin décembre, avant cela nous avions des cartes de test blanches pour tester nos premières fonctionnalités comme le Swipper.

## Difficultés rencontrées

L’installation d’Expo se fait avec NodeJS, après avoir réussi à tous l’installer, nous nous sommes rendus compte d’un problème au lancement du projet. Il faut être sur le même réseau local (client et serveur) afin de pouvoir lancer le programme sur le mobile.
Nous avons trouvé un Swipper dans une bibliothèque libre, la difficulté a été de le faire correspondre aux éléments déjà développés comme les cartes. En les mettant dans une même div et en jouant avec la fonction render() nous sommes parvenus à faire bouger nos cartes test.
La « mise en page » et le placement des éléments où on voulait fut laborieuse car nous l’effectuant avec des règles manuelles. Ainsi l’alignement des jauges, du texte et du tas de cartes fut placer en statique après de nombreux essais pour savoir qu’elle placement convenait le mieux.

## API Routes :

Adresse à pointer : http://69493f49.ngrok.io
Cartes : http://69493f49.ngrok.io/cards
Personnages : http://69493f49.ngrok.io/characters
Fins de jeu : http://69493f49.ngrok.io/ends
Objets : http://69493f49.ngrok.io/objects

Pour récupérer une carte, un personnage, etc en particulier, ajouter le nom à l'url. ( exemple : http://69493f49.ngrok.io/cards/the-skeleton )

